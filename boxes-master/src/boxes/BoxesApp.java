package boxes;

import java.util.Random;

import javafx.application.Application;
import javafx.stage.Stage;
import boxes.BoxesContract.BoxesModel;
import boxes.BoxesContract.BoxesPresenter;
import boxes.BoxesContract.BoxesView;
import boxes.impl.BoxesModelImpl;
import boxes.impl.BoxesPresenterImpl;
import boxes.impl.BoxesViewImpl;

public class BoxesApp extends Application {

    public static final int SIZE = 5;

    private BoxesView v;
    private BoxesPresenter p;
    private BoxesModel m;

    @Override
    public void start(Stage primaryStage) throws Exception {

        p = new BoxesPresenterImpl();
        m = new BoxesModelImpl(SIZE);
        v = new BoxesViewImpl(primaryStage, SIZE);

        p.setView(v);
        p.setModel(m);
        v.setPresenter(p);

        m.start(new Random().nextInt(2) + 1);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
