package boxes;

public interface BoxesContract {

	interface BoxesView {
		
		void setPresenter(BoxesPresenter p);
		
		// other methods called by presenter here
		
		void viewEdge(Edge e, boolean view);
		
		void drawEdge(boolean player1, Edge e);
		void drawBox(boolean player1, int x, int y);
		void say(String text);
		void setScore(int player, int score);
		void setTurn(int player);
	}
	
	interface BoxesPresenter extends BoxesModelListener {
		
		void setView(BoxesView v);		
		void setModel(BoxesModel m);
		
		// other methods called by view here
		
		void enterEvent(Edge e);
		void leaveEvent(Edge e);
		void clickEvent(Edge e);
	}
	
	interface BoxesGame {
		
		// methods called by presenter here
		
		int getSize();
		int getWinner();
		int getTurn();
		int getScore(int player);
		int getBox(int x, int y);
		int getEdge(Edge e);		
		boolean isOver();		
		void start(int turn);
		boolean play(int player, Edge e);
	}
	
	interface BoxesModel extends BoxesGame {
		
		boolean addListener(BoxesModelListener listener);
		boolean removeListener(BoxesModelListener listener);
	}
	
	interface BoxesModelListener {

		void edgeEvent(int player, Edge e);
		void scoreEvent(int player, int x, int y);
		void overEvent();
		void turnEvent(int player);
	}
}
