package boxes.impl;

import javafx.stage.Stage;
import boxes.BoxesContract.BoxesPresenter;
import boxes.BoxesContract.BoxesView;
import boxes.Edge;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;

public class BoxesViewImpl implements BoxesView {

    private BoxesPresenter presenter;

    private int size;

    public BoxesViewImpl(Stage primaryStage, int size) {
        this.size = size;
        initUI(primaryStage);		
    }

    @Override
    public void setPresenter(BoxesPresenter p) {
        this.presenter = p;
    }

    // INTERFACE	
    @Override
    public void viewEdge(Edge e, boolean view) {
        throw new RuntimeException("no implementat!");
    }

    @Override
    public void drawEdge(boolean player1, Edge e) {
        throw new RuntimeException("no implementat!");
    }

    @Override
    public void drawBox(boolean player1, int x, int y) {
        throw new RuntimeException("no implementat!");
    }

    @Override
    public void say(String text) {
        throw new RuntimeException("no implementat!");
    }

    @Override
    public void setScore(int player, int score) {
        throw new RuntimeException("no implementat!");
    }

    @Override
    public void setTurn(int player) {
        throw new RuntimeException("no implementat!");
    }

    // DRAW
    private void initUI(Stage primaryStage) {

        Canvas canvas = new Canvas(500, 500);

        GraphicsContext gc = canvas.getGraphicsContext2D();

        Pane root = new Pane();

    

        root.getChildren().add(canvas);
        Scene scene = new Scene(root, 500, 500);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Juego Boxes ");
        primaryStage.show();
    }
}
