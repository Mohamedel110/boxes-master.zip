package boxes.impl;

import boxes.BoxesContract.BoxesModel;
import boxes.BoxesContract.BoxesPresenter;
import boxes.BoxesContract.BoxesView;
import boxes.Edge;

public class BoxesPresenterImpl implements BoxesPresenter {

	private BoxesView view;
	private BoxesModel model;
	
	public BoxesPresenterImpl() {
	}
	
	@Override
	public void setView(BoxesView v) {		
		this.view = v;
	}

	@Override
	public void setModel(BoxesModel m) {		
		this.model = m;
		this.model.addListener(this);
	}
		
	// EVENTS FROM VIEW

	@Override
	public void enterEvent(Edge e) {
		throw new RuntimeException("no implementat!");
	}

	@Override
	public void leaveEvent(Edge e) {
		throw new RuntimeException("no implementat!");
	}

	@Override
	public void clickEvent(Edge e) {
		throw new RuntimeException("no implementat!");
	}

	@Override
	public void scoreEvent(int player, int x, int y) {
		throw new RuntimeException("no implementat!");
	}
	
	@Override
	public void edgeEvent(int player, Edge e) {
		throw new RuntimeException("no implementat!");
	}

	@Override
	public void overEvent() {
		throw new RuntimeException("no implementat!");
	}

	@Override
	public void turnEvent(int player) {	
		throw new RuntimeException("no implementat!");
	}

}
