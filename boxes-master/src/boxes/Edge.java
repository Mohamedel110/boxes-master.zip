package boxes;

public class Edge {
	
	private int x, y;
	private boolean isH;

	public Edge(boolean isH, int x, int y) {
		this.x = x;
		this.y = y;
		this.isH = isH;
	}

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean isIsH() {
        return isH;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setIsH(boolean isH) {
        this.isH = isH;
    }
        
        

        
}
